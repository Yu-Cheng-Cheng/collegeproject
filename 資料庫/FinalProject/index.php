﻿<?php
$con = mysqli_connect("localhost","root","12345678");//連結伺服器
mysqli_select_db($con, "db_project1");//選擇資料庫
mysqli_query($con, "set names utf8");//以utf8讀取資料，讓資料可以讀取中文
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>資料庫網頁建置</title>

	<style>
		ul.pagination {
			display: inline-block;
			padding: 0;
			margin: 0;
		}

		ul.pagination li {display: inline;}

		ul.pagination li a {
			color: black;
			float: left;
			padding: 8px 16px;
			text-decoration: none;
			transition: background-color .3s;
			border: 1px solid #ddd;
		}

		.pagination li:first-child a {
			border-top-left-radius: 5px;
			border-bottom-left-radius: 5px;
		}

		.pagination li:last-child a {
			border-top-right-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		ul.pagination li a.active {
			background-color: #4CAF50;
			color: white;
			border: 1px solid #4CAF50;
		}

		ul.pagination li a:hover:not(.active) {background-color: #ddd;}
				
				
		
		#tab
		{
			width: 100%;
			background: #728ca3;
			border: solid 0px #8f4f06;
		}
		/* 頁籤ul */
		#tab > ul
		{
			overflow: hidden;
			margin: 0;
			padding: 10px 20px 0 20px;
		}
		#tab > ul > li
		{
			list-style-type: none;
		}
		#tab > ul > li > a
		{
			text-decoration: none;
			font-size: 22px;
			color: #e6eff3;
			float: left;
			padding: 10px;
			margin-left: 5px;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
		
		}
		 
		/*頁籤div內容*/
		#tab > div
		{
			clear:both;
			padding:0 15px;
			height:0;
			overflow:hidden;
			visibility:hidden;
			-webkit-transition:all .4s ease-in-out;
			-moz-transition:all .4s ease-in-out;
			-ms-transition:all .4s ease-in-out;
			-o-transition:all .4s ease-in-out;
			transition:all .4s ease-in-out;
		}
		 
		/* span:target */
		#tab-1:target,
		#tab-2:target,
		#tab-3:target,
		#tab-4:target,
		#tab-5:target
		{
		  border: solid 0px red;
		}
		 
		/*第一筆的底色*/
		/* span:target ~ #tab > ul li:first-child a
		{
			background:#1caa5d;
		}
		span:target ~ #tab > div:first-of-type 
		{
			visibility:hidden;
			height:0;
			padding:0 15px;
		} */ 
		  
		/*頁籤變換&第一筆*/
		/* span ~ #tab > ul li:first-child a, */
		#tab-1:target ~ #tab > ul li a[href$="#tab-1"],
		#tab-2:target ~ #tab > ul li a[href$="#tab-2"],
		#tab-3:target ~ #tab > ul li a[href$="#tab-3"],
		#tab-4:target ~ #tab > ul li a[href$="#tab-4"],
		#tab-5:target ~ #tab > ul li a[href$="#tab-5"]
		{
		  background: #f2ece1;
		  color: #333;
		}
		 
		/*頁籤內容顯示&第一筆*/
		/* span ~ #tab > div:first-of-type, */
		#tab-1:target ~ #tab > div.tab-content-1,
		#tab-2:target ~ #tab > div.tab-content-2,
		#tab-3:target ~ #tab > div.tab-content-3,
		#tab-4:target ~ #tab > div.tab-content-4,
		#tab-5:target ~ #tab > div.tab-content-5
		{
			visibility:visible;
			height:auto;
			background :#f2ece1;
		}
		
		
		table.TB_COLLAPSE 
		{
		  width:100%;
		  border-collapse:collapse;
		}
		table.TB_COLLAPSE thead th 
		{
		  padding:5px 0px;
		  color:#fff;
		  background-color:#b87070;
		}
		table.TB_COLLAPSE tbody td 
		{
		  padding:5px 0px;
		  color:#555;
		  text-align:center;
		  background-color:#fff;
		  border-bottom:1px solid #915957;
		}
		
		img {
			width:200px;
			height:150px;
		}
		
		
		tr {
			background:#fff;
			box-shadow: 10px 10px 5px #888888;
		}
		
		legend{
			
			font-size:20px;
			font-weight:bold;
			background :#b87070;
			border: solid 0px #333;
			color: #fff;
			
			
			border-top-left-radius: 5px;
			border-bottom-left-radius: 5px;
			border-top-right-radius: 5px;
			border-bottom-right-radius: 5px;
		}
		
		fieldset{
			width:435px;
			background :#fff;
			
		}
		
		body{
			
			margin:0;
			
		}
		
		p.line{
			
			border: solid 1px #b87070;
		}
	</style>
</head>

<body>
 
	<span id="tab-1"></span>
	<span id="tab-2"></span>
	<span id="tab-3"></span>
	<span id="tab-4"></span>
	<span id="tab-5"></span>
  
	<div id="tab">
	
		<!-- 頁籤按鈕 -->
		<ul>
		  <li><a href="#tab-1">員工基本資料表</a></li>
		  <li><a href="#tab-2">國家資料表</a></li>
		  <li><a href="#tab-3">員工派駐資料表</a></li>
		  <li><a href="#tab-4">眷屬資料表</a></li>
		  <li><a href="#tab-5">跨資料表整合介面</a></li>
		</ul>
		<!-- 頁籤的內容區塊 -->
		
		<div class="tab-content-1">			
			<!-- <p>內容-1</p> -->
			
			<form name="form1" method="post" action="">
				<p>
					。輸入身分證字號以查詢：
					<input name="ssn" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			<form name="form2" method="post" action="">
				<p>
					。輸入職等以查詢人數：　　　
					<input name="level" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			
			<p>
				<?php
				
					if(isset($_POST['ssn']))
					{
						if($_POST['ssn']!='')
						{
							$ssn = $_POST['ssn'];
							$data = mysqli_query($con, "select * 
													  from employee 
													  where ssn like '%$ssn%'"); 
						}
						else
						{
							$data = mysqli_query($con, "select * 
												  from employee");
							
						}
					}
					else
					{
						$data = mysqli_query($con, "select * 
										  from employee");
					} 
				?>
			</p>
			
			<?php
				$p=mysqli_query($con, "select Count(Ssn) from employee");
				$pResult = mysqli_fetch_row($p);	

				$s=mysqli_query($con, "select AVG(salary) from employee");
				$sResult = mysqli_fetch_row($s);
 
				$a=mysqli_query($con, "select AVG((YEAR(CURDATE())-YEAR(born_date))) from employee");
				$aResult = mysqli_fetch_row($a);
					
				$s=mysqli_query($con, "select SUM(salary) from employee");
				$sResult = mysqli_fetch_row($s);				
				
				if(isset($_POST['level']))
				{
					$degree = $_POST['level'];
					$l = mysqli_query($con, "select Count(Ssn) from employee where degree like '%$degree%'");
					$lResult = mysqli_fetch_row($l);	
				}
				else
				{
					$lResult[0] = '0';			
					$degree = '';
				}
			?>
			<p>　
			<?php echo $degree ?> 職等人數：<?php echo $lResult[0] ?>
			　　
			</p>
			<p class = "line"></p>
			<p>
			人數：<?php echo $pResult[0] ?>
			　　
			平均薪資：<?php echo $sResult[0] ?>
			　　
			平均年齡：<?php echo $aResult[0] ?>
			　　
			</p>
			<p>
			全年總薪資：<?php echo $sResult[0] * 12 ?>
			　　
			每月總薪資：<?php echo $sResult[0] ?>
			　　
			每周總薪資：<?php echo $sResult[0] /30 *7 ?>
			　　
			</p>
			
			
					
			<table class = "TB_COLLAPSE" >
				<thead>
					<tr>
						<th width="70" >姓名</th>
						<th >身分證字號</th>
						<th >職等</th>
						<th >薪資</th>
						<th >電話</th>
						<th >性別</th>
						<th >出生年月日</th>
						<th >住址</th>
						<th >照片</th>
						<th >狀態</th>
					</tr>
				</thead>
				
				<?php
				for($i = 1; $i <= mysqli_num_rows($data); $i++)
				{
					$rs = mysqli_fetch_row($data);
				?>		
					<tbody>
						<tr>
							<td><?php echo $rs[0]?></td>
							<td><?php echo $rs[1]?></td>
							<td><?php echo $rs[2]?></td>
							<td><?php echo $rs[3]?></td>
							<td><?php echo $rs[4]?></td>
							<td><?php echo $rs[5]?></td>
							<td><?php echo $rs[6]?></td>
							<td><?php echo $rs[7]?></td>
							<td ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $rs[8] ).'"/>';?></td>
							<td><?php echo $rs[9]?></td>
						</tr>
					</tbody>
				<?php
				}
				?>
			</table>
			<p></p>
			
			
			<form class= 'adit' name = "employee" method="post" action="" enctype="multipart/form-data"> 
			
				<fieldset>
				<legend>　員工基本資料　</legend>
			    姓名:　　　 <input type="text" name="name2">
			    <br>		
			    身分證字號: <input type="text" name="ssn2">
			    <br>			
				職等:　　　 <input type="text" name="degree2">
			    <br>			
				薪資:　　　 <input type="text" name="salary2" >
			    <br>			
				電話:　　　 <input type="text" name="phone2">
			    <br>		
			    性别:　　　
				<input type="radio" name="gender2" value ="女">女			
				<input type="radio" name="gender2" value ="男">	男
			    <br>		
				出生年月日: <input type="date" name="born_date2" >
			    <br>		
				住址: 　　　<input type="text" name="address2">
			    <br>		
				照片: 　　　<input type="file"  name="upfile" accept="image/gif, image/jpeg, image/png" >
			    <br>
				執行:　　　
				<input type="radio" name="act" value ="add">新增			
				<input type="radio" name="act" value ="update">更新
				<input type="radio" name="act" value ="delete">刪除
				<input type="radio" name="act" value ="restore">還原
			    <br>		
				
			    <input type="submit" value="執行"> 
				<input type="reset">
				</fieldset>
			
			</form>
			
			
			<?php	 
			// 创建连接
			if(isset($_POST['ssn2']))
			{
				$a = $_POST["name2"];
			
				$b = $_POST["ssn2"];

				$c = $_POST["degree2"];

				$d = $_POST["salary2"];
		
				$e = $_POST["phone2"];
		
				$f = $_POST["gender2"];
			
				$g = $_POST["born_date2"];
			
				$h = $_POST["address2"];
				
				$picture2 =addslashes(file_get_contents($_FILES['upfile']['tmp_name']));
				
				if($_POST["act"] == 'add')
				{
					$sql = "INSERT INTO employee (name,ssn,degree,salary,phone,gender,born_date,address,picture)
					VALUES ('$a','$b','$c','$d',$e,'$f','$g','$h','{$picture2}')";
					
					$result=mysqli_query($con,$sql) or die('MySQL query add error');
				}
				elseif($_POST["act"] == 'update')
				{
					$sql = "UPDATE employee 
					SET name = '$a',ssn = '$b',degree = '$c',salary = '$d',phone = '$e',gender = '$f',born_date = '$g',address = '$h',picture = '{$picture2}' 
					WHERE ssn = '$b'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query update error');
				}
				elseif($_POST["act"] == 'delete')
				{
					$sql = "UPDATE employee 
					SET Estatus = '刪除'
					WHERE ssn = '$b'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query delete error');
				}
				elseif($_POST["act"] == 'restore')
				{
					$sql = "UPDATE employee 
					SET Estatus = '正常'
					WHERE ssn = '$b'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query restore error');
				}
				
			}
			
			?>
			<p></p>
			
		</div>
		
		<div class="tab-content-2">
			<!-- <p>內容-2</p> -->
			
			
			<form name="form1" method="post" action="">
				<p>
					。輸入國家代碼以查詢：
					<input name="c_code" type="text"  />
					<input type='submit'>
				</p>
			</form>
			
			<p>
				<?php
				
					if(isset($_POST['c_code']))
					{
						if($_POST['c_code']!='')
						{
							$c_code = $_POST['c_code'];
							$data = mysqli_query($con, "select * 
													  from country 
													  where c_code like '%$c_code%'"); 
						}
						else
						{
							$data = mysqli_query($con, "select * 
												  from country");
							
						}
					}
					else
					{
						$data = mysqli_query($con, "select * 
										  from country");
					} 
				?>
			</p>
			
			<form name="form2" method="post" action="">
				<p>
					。輸入洲名以查詢有無邦交國：
					<input name="state" type="text"  />
					<input type='submit'>
				</p>
			</form>
			
			
			<?php
				if(isset($_POST['state']))
				{
					$c_state = $_POST['state'];
					$sd = mysqli_query($con, "select Count(c_deplomatic) from country where c_state like '%$c_state%' && c_deplomatic = '是' ");
					$snd = mysqli_query($con, "select Count(c_deplomatic) from country where c_state like '%$c_state%' && c_deplomatic = '否' ");
					
					$sdResult = mysqli_fetch_row($sd);	
					$sndResult = mysqli_fetch_row($snd);	
				}
				else
				{
					$c_state = '';
					$sdResult[0] = '0';				
					$sndResult[0] = '0';									
				}
			?>
			
			
			<p>　
			<?php echo $c_state ?> 擁有邦交國個數：<?php echo $sdResult[0] ?>
			　　
			<?php echo $c_state ?> 擁有非邦交國個數：<?php echo $sndResult[0] ?>
			　　
			</p>		
			
			<form name="form3" method="post" action="">
				<p>
					。輸入國家名稱以查詢其國民人數：
					<input name="name" type="text"  />
					<input type='submit'>
				</p>
			</form>
			
			<?php
				if(isset($_POST['name']))
				{
					$c_name = $_POST['name'];
					$n = mysqli_query($con, "select c_population from country where c_name like '%$c_name%'");
				
					$nResult = mysqli_fetch_row($n);	
				}
				else
				{
					$c_name = '';
					$nResult[0] = '0';									
				}
			?>
			
			
			<p>　
			<?php echo $c_name ?> 國民人數：<?php echo $nResult[0] ?>
			　　
			</p>		
			
			
			
			<p class = "line"></p>
			<?php
				$d = mysqli_query($con, "select Count(c_deplomatic) from country where c_deplomatic = '是'");
				$dResult = mysqli_fetch_row($d);	

				$nd = mysqli_query($con, "select Count(c_deplomatic) from country where c_deplomatic =  '否'");
				$ndResult = mysqli_fetch_row($nd);
				
				$cp = mysqli_query($con, "select Sum(c_population) from country where c_deplomatic =  '是'");
				$cpResult = mysqli_fetch_row($cp);
				
				$cnp = mysqli_query($con, "select Sum(c_population) from country where c_deplomatic =  '否'");
				$cnpResult = mysqli_fetch_row($cnp);
			?>
			
			<p>　
			總邦交國個數：<?php echo $dResult[0] ?>
			
			總人口數: <?php echo $cpResult[0] ?>
			　　
			總非邦交國個數：<?php echo $ndResult[0] ?>
			
			總人口數: <?php echo $cnpResult[0] ?> 
			　　
			</p>
					
			<table class = "TB_COLLAPSE">
				<thead>
					<tr>
						<th width="" >國家代碼</th>
						<th >國家名稱</th>
						<th >所屬洲名</th>
						<th >元首姓名</th>
						<th >外交部長姓名	</th>
						<th >聯絡人姓名	</th>
						<th >人口數	</th>
						<th >領土面積(km^3)</th>
						<th >連絡電話</th>
						<th >是否為邦交國</th>
						<th >　狀態　</th>
					</tr>
				</thead>
				<?php
				for($i = 1; $i <= mysqli_num_rows($data); $i++){
					$rs = mysqli_fetch_row($data);
				?>				
					<tr>
					<td><?php echo $rs[0]?></td>
					<td><?php echo $rs[1]?></td>
					<td><?php echo $rs[2]?></td>
					<td><?php echo $rs[3]?></td>
					<td><?php echo $rs[4]?></td>
					<td><?php echo $rs[5]?></td>
					<td><?php echo $rs[6]?></td>
					<td><?php echo $rs[7]?></td>
					<td><?php echo $rs[8]?></td>
					<td><?php echo $rs[9]?></td>
					<td><?php echo $rs[10]?></td>
					</tr>
				<?php
				}
				?>
			</table>
			
			<p></p>
		
			<form class="adit" name = "country" method="post" action="" enctype="multipart/form-data"> 
			
				<fieldset>
				<legend>　新增國家基本資料　</legend>
			    國家代碼:　　　<input type="text" name="c_code2">
			    <br>		
			    國家名稱:　　　<input type="text" name="c_name2">
			    <br>			
				所屬洲名:　　　<input type="text" name="c_state2">
			    <br>			
				元首姓名:　　　<input type="text" name="c_president2" >
			    <br>			
				外交部長姓名:　<input type="text" name="c_Fminister2">
			    <br>		
			    聯絡人姓名:　　<input type="text" name="c_Fcontect2">
			    <br>	
				人口數:　　　　<input type="text" name="c_population2" >
			    <br>		
				領土面積(km^3): <input type="text" name="c_area2">
			    <br>		
				連絡電話:　　　<input type="text" name="c_phone2">
			    <br>
				是否為邦交國:
				<input type="radio" name="c_deplomatic2" value ="是">是		
				<input type="radio" name="c_deplomatic2" value ="否">否
			    <br>
				執行:　　　　
				<input type="radio" name="act" value ="add">新增			
				<input type="radio" name="act" value ="update">更新
				<input type="radio" name="act" value ="delete">刪除
				<input type="radio" name="act" value ="restore">還原
			    <br>		
				
			    <input type="submit" value="執行"> 
				<input type="reset">
				</fieldset>
				
			</form>
		
			
			<?php	 
			// 创建连接
			if(isset($_POST['c_code2']))
			{
				$a = $_POST["c_code2"];
			
				$b = $_POST["c_name2"];

				$c = $_POST["c_state2"];

				$d = $_POST["c_president2"];
		
				$e = $_POST["c_Fminister2"];
		
				$f = $_POST["c_Fcontect2"];
			
				$g = $_POST["c_population2"];
			
				$h = $_POST["c_area2"];
			
				$i = $_POST["c_phone2"];
				
				$j = $_POST["c_deplomatic2"];
				
				if($_POST["act"] == 'add')
				{
					$sql = "INSERT INTO country(c_code,c_name,c_state,c_president,c_Fminister,c_Fcontect,c_population,c_area,c_phone,c_deplomatic)
					VALUES ('$a','$b','$c','$d','$e','$f','$g','$h','$i','$j')";
					
					$result = mysqli_query($con,$sql)  or die('MySQL query add error');
				}
				elseif($_POST["act"] == 'update')
				{
					$sql = "UPDATE country 
					SET c_code = '$a',c_name = '$b',c_state = '$c',c_president = '$d',c_Fminister = '$e',c_Fcontect = '$f',c_population = '$g',c_area = '$h',c_phone = '$i',  c_deplomatic ='$j'
					WHERE c_code = '$a'";
								
					$result=mysqli_query($con,$sql) or die('MySQL query update error');
				}
				elseif($_POST["act"] == 'delete')
				{
					$sql = "UPDATE country 
					SET Cstatus = '亡國'
					WHERE c_code = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query delete error');
				}
				elseif($_POST["act"] == 'restore')
				{
					$sql = "UPDATE country 
					SET Cstatus = '正常'
					WHERE c_code = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query restore error');
				}
			}
			
			?>
			<p></p>
		
		</div>

		<div class="tab-content-3">
			<!-- <p>內容-3</p> -->
			
						<form name="form1" method="post" action="">
				<p>
					。輸入身分證字號以查詢：
					<input name="Wssn" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			<p>
				<?php
				
					if(isset($_POST['Wssn']))
					{
						if($_POST['Wssn']!='')
						{
							$Wssn = $_POST['Wssn'];
							$data = mysqli_query($con, "select * 
													  from workfor 
													  where Wssn like '%$Wssn%'"); 
						}
						else
						{
							$data = mysqli_query($con, "select * 
												  from workfor");
							
						}
					}
					else
					{
						$data = mysqli_query($con, "select * 
										  from workfor");
					} 
				?>
			</p>
			
			<form name="form1" method="post" action="">
				<p>
					。輸入國家代碼以查詢總派駐員工人數：
					<input name="Wcountry_code" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			<?php
				if(isset($_POST['Wcountry_code']))
				{
					$Wcountry_code = $_POST['Wcountry_code'];
					$wc = mysqli_query($con, "select Count(Wssn) from workfor where Wcountry_code like '%$Wcountry_code%'");
					
					$wcResult = mysqli_fetch_row($wc);	
				
				}
				else
				{
					$Wcountry_code = '';
					$wcResult[0] = '0';													
				}
			?>
			
			
			<p>　
			<?php echo $Wcountry_code ?> 派駐員工人數: <?php echo $wcResult[0] ?>
			　　
			</p>
			
			<table class = "TB_COLLAPSE">
				<thead>
					<tr>
						<th width="" >員工身分證字號</th>
						<th >派駐國家代碼</th>
						
						<th >到職日期</th>
						<th >大使(代表)名稱	</th>
						<th >狀態</th>
					</tr>
				</thead>
				<?php
				for($i = 1; $i <= mysqli_num_rows($data); $i++){
					$rs = mysqli_fetch_row($data);
				?>				
					<tr>
					<td><?php echo $rs[0]?></td>
					<td><?php echo $rs[1]?></td>
					
					<td><?php echo $rs[3]?></td>
					<td><?php echo $rs[4]?></td>
					<td><?php echo $rs[5]?></td>
					</tr>
				<?php
				}
				?>
			</table>
			
			<p></p>
		
			<form class="adit" name = "country" method="post" action="" enctype="multipart/form-data"> 
			
				<fieldset>
				<legend>　新增員工派駐資料　</legend>
			    員工身分證字號:<input type="text" name="Wssn2">
			    <br>		
			    派駐國家代碼:　<input type="text" name="Wcountry_code2">
			    <br>			
						
				到職日期:　　　<input type="date" name="end_date2" >
			    <br>			
				大使(代表)名稱:<input type="text" name="Wdeputy2">
				<br>
				執行:　　　　　
				<input type="radio" name="act" value ="add">新增			
				<input type="radio" name="act" value ="update">更新
				<input type="radio" name="act" value ="delete">刪除
				<input type="radio" name="act" value ="restore">還原
			    <br>		
				
			    <input type="submit" value="執行"> 
				<input type="reset">
				</fieldset>
				
			</form>
		
			
			<?php	 
			// 创建连接
			if(isset($_POST['Wssn2']))
			{
				$a = $_POST["Wssn2"];
			
				$b = $_POST["Wcountry_code2"];

				$d = $_POST["end_date2"];
		
				$e = $_POST["Wdeputy2"];
		
				if($_POST["act"] == 'add')
				{
					$sql = "INSERT INTO workfor(Wssn,Wcountry_code,end_date,Wdeputy)
					VALUES ('$a','$b','$d','$e')";
					
					$result = mysqli_query($con,$sql)  or die('MySQL query add error');
				}
				elseif($_POST["act"] == 'update')
				{
					$sql = "UPDATE workfor 
					SET Wssn = '$a',Wcountry_code = '$b',end_date = '$d',Wdeputy = '$e'
					WHERE Wssn = '$a'";
								
					$result=mysqli_query($con,$sql) or die('MySQL query update error');
				}
				elseif($_POST["act"] == 'delete')
				{
					$sql = "UPDATE workfor 
					SET Wstatus = '離職'
					WHERE Wssn = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query delete error');
				}
				elseif($_POST["act"] == 'restore')
				{
					$sql = "UPDATE workfor 
					SET Wstatus = '正常'
					WHERE Wssn = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query restore error');
				}
			}
			
			?>
			<p></p>
		
		</div>

		<div class="tab-content-4">
			<!-- <p>內容-4</p> -->
			
			
			<form id="form1" name="form1" method="post" action="">
				<p>
					。輸入員工身分證字號以查詢：
					<input name="d_Essn" type="text" value="<?php $d_Essn?>" />
					<input type='submit'>
				</p>
			</form>
			
			<p>
				<?php
				
					if(isset($_POST['d_Essn']))
					{
						if($_POST['d_Essn']!='')
						{
							$d_Essn = $_POST['d_Essn'];
							$data = mysqli_query($con, "select * 
													  from dependent 
													  where d_Essn like '%$d_Essn%'"); 
						}
						else
						{
							$data = mysqli_query($con, "select * 
												  from dependent");
							
						}
					}
					else
					{
						$data = mysqli_query($con, "select * 
										  from dependent");
					} 
				?>
			</p>
			
			<form name="form2" method="post" action="">
				<p>
					。輸入眷屬身分證字號以查詢：
					<input name="d_ssn" type="text" value="<?php $d_ssn?>" />
					<input type='submit'>
				</p>
			</form>
			
			<p>
				<?php
				
					if(isset($_POST['d_ssn']))
					{
						if($_POST['d_ssn']!='')
						{
							$d_ssn = $_POST['d_ssn'];
							$data = mysqli_query($con, "select * 
													  from dependent 
													  where d_ssn like '%$d_ssn%'"); 
						}
						else
						{
							$data = mysqli_query($con, "select * 
												  from dependent");
							
						}
					}
					else
					{
						$data = mysqli_query($con, "select * 
										  from dependent");
					} 
				?>
			</p>
					
			<?php
				$db=mysqli_query($con, "select Avg((YEAR(CURDATE())-YEAR(d_born_date))) from dependent");
				$dbResult = mysqli_fetch_row($db);	
				
				$dbm=mysqli_query($con, "select Avg((YEAR(CURDATE())-YEAR(d_born_date))) from dependent where d_gender = '男'");
				$dbmResult = mysqli_fetch_row($dbm);	
				
				$dbf=mysqli_query($con, "select Avg((YEAR(CURDATE())-YEAR(d_born_date))) from dependent where d_gender = '女' ");
				$dbfResult = mysqli_fetch_row($dbf);	
				
				$dpm=mysqli_query($con, "select Count(d_ssn ) from dependent where d_gender = '男' ");
				$dpmResult = mysqli_fetch_row($dpm);	
				
				$dpf=mysqli_query($con, "select Count(d_ssn) from dependent where d_gender = '女' ");
				$dpfResult = mysqli_fetch_row($dpf);	
				
			?>
			<p>
			眷屬平均年齡：<?php echo $dbResult[0] ?>
			　　
			男眷屬平均年齡：<?php echo $dbmResult[0] ?>
			
			共 <?php echo $dpmResult[0] ?> 人
			　　
			女眷屬平均年齡：<?php echo $dbfResult[0] ?>
			
			共 <?php echo $dpfResult[0] ?> 人
			　　
			</p>
			
					
			<table class = "TB_COLLAPSE">
				<thead>
					<tr>
						<th width="" >員工身分證字號</th>
						<th >眷屬身分證字號</th>
						<th >眷屬姓名</th>
						<th >眷屬性別</th>
						<th >與員工關係	</th>
						<th >出生年月日	</th>
						<th >狀態</th>
					</tr>
				</thead>
				<?php
				for($i = 1; $i <= mysqli_num_rows($data); $i++){
					$rs = mysqli_fetch_row($data);
				?>				
					<tr>
					<td><?php echo $rs[0]?></td>
					<td><?php echo $rs[1]?></td>
					<td><?php echo $rs[2]?></td>
					<td><?php echo $rs[3]?></td>
					<td><?php echo $rs[4]?></td>
					<td><?php echo $rs[5]?></td>
					<td><?php echo $rs[6]?></td>
					</tr>
				<?php
				}
				?>
			</table>
			
			<p></p>
		
			<form class="adit" name = "country" method="post" action="" enctype="multipart/form-data"> 
			
				<fieldset>
				<legend>　新增眷屬基本資料　</legend>
			    員工身分證字號:<input type="text" name="d_Essn2">
			    <br>		
				眷屬身分證字號:<input type="text" name="d_ssn2">
			    <br>	
			    眷屬姓名:　　　<input type="text" name="d_name2">
			    <br>			
				眷屬性別:　　　
				<input type="radio" ]=-----------[?：name="d_gender2" value ="女">   女			
				<input type="radio" name="d_gender2" value ="男">	男
			    <br>			
				與員工關係:　　<input type="text" name="d_relationship2" >
			    <br>			
				出生年月日:　　<input type="date" name="d_born_date2">
			    <br>		
			    
				執行:　  　　　
				<input type="radio" name="act" value ="add">新增			
				<input type="radio" name="act" value ="update">更新
				<input type="radio" name="act" value ="delete">刪除
				<input type="radio" name="act" value ="restore">還原
			    <br>		
				
			    <input type="submit" value="執行"> 
				<input type="reset">
				</fieldset>
				
			</form>
		
			
			<?php	 
			// 创建连接
			if(isset($_POST['d_Essn2']))
			{
				$a = $_POST["d_Essn2"];
			
				$b = $_POST["d_ssn2"];

				$c = $_POST["d_name2"];

				$d = $_POST["d_gender2"];
		
				$e = $_POST["d_relationship2"];
		
				$f = $_POST["d_born_date2"];
			
				
				if($_POST["act"] == 'add')
				{
					$sql1 = "INSERT INTO dependent(d_Essn,d_ssn,d_name,d_gender,d_relationship,d_born_date)
					VALUES ('$a','$b','$c','$d','$e','$f')";
					
					$result = mysqli_query($con,$sql1)  or die('MySQL query add error');
				}
				elseif($_POST["act"] == 'update')
				{
					$sql = "UPDATE dependent 
					SET d_Essn = '$a',d_ssn = '$b',d_name = '$c',d_gender = '$d',d_relationship = '$e',d_born_date = '$f'
					WHERE d_Essn = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query update error');
				}
				elseif($_POST["act"] == 'delete')
				{
					$sql = "UPDATE dependent 
					SET Dstatus = '解除關係'
					WHERE d_Essn = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query delete error');
				}
				elseif($_POST["act"] == 'restore')
				{
					$sql = "UPDATE dependent 
					SET Dstatus = '正常'
					WHERE d_Essn = '$a'";
							
					$result=mysqli_query($con,$sql) or die('MySQL query restore error');
				}
			}
			
			?>
			<p></p>
		
		</div>
		
		<div class="tab-content-5">
			<!-- <p>內容-5</p> -->
			
			<form name="form1" method="post" action="">
				<p>
					。輸入國家名稱以查詢 30 歲以上 派駐員工人數：
					<input name="Country_name" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			<?php
				if(isset($_POST['Country_name']))
				{
					$Country_name = $_POST['Country_name'];
					$cnt = mysqli_query($con, "select Count(ssn) from employee, country, workfor where (YEAR(CURDATE())-YEAR(born_date)) >= 30 AND ssn = Wssn AND Wcountry_code = c_code AND c_name like '%$Country_name%'");
					
					$cntResult = mysqli_fetch_row($cnt);	
				
				}
				else
				{
					$Country_name = '';
					$cntResult[0] = '0';													
				}
			?>
			
			
			<p>　
			<?php echo $Country_name ?> 30歲以上 派駐員工人數: <?php echo $cntResult[0] ?>
			　　
			</p>
			
			<form name="form2" method="post" action="">
				<p>
					。輸入洲名以查詢 30 歲以上 派駐員工人數：
					<input name="state_name" type="text" />
					<input type='submit'>
				</p>
			</form>
			
			<?php
				if(isset($_POST['state_name']))
				{
					$state_name = $_POST['state_name'];
					$cnt1 = mysqli_query($con, "select Count(ssn) from employee, country, workfor where (YEAR(CURDATE())-YEAR(born_date)) >= 30 AND ssn = Wssn AND Wcountry_code = c_code AND c_state like '%$state_name%'");
					
					$cnt1Result = mysqli_fetch_row($cnt1);	
				
				}
				else
				{
					$state_name = '';
					$cnt1Result[0] = '0';													
				}
			?>
			
			
			<p>　
			<?php echo $state_name ?> 30歲以上 派駐員工人數: <?php echo $cnt1Result[0] ?>
			　　
			</p>
			
			<?php
				$dage = mysqli_query($con, "select AVG((YEAR(CURDATE())-YEAR(d_born_date))) from employee, dependent where (YEAR(CURDATE())-YEAR(born_date)) >= 30 AND ssn = d_Essn ");
				$dageResult = mysqli_fetch_row($dage);	

				$dpop = mysqli_query($con, "select Count(d_ssn) from employee, dependent where (YEAR(CURDATE())-YEAR(born_date)) >= 30 AND ssn = d_Essn ");
				$epop = mysqli_query($con, "select Count(ssn) from employee where (YEAR(CURDATE())-YEAR(born_date)) >= 30  ");
				
				$dpopResult = mysqli_fetch_row($dpop);
				$epopResult = mysqli_fetch_row($epop);
				
			?>
			
			<p>
			。30歲以上員工之平均眷屬年齡: <?php echo $dageResult[0] ?>
			　　
			。30歲以上員工之平均眷屬人數: <?php echo $dpopResult[0]/ $epopResult[0]  ?>
			　　
			</p>
			
		</div>
	
		
	</div>
 
</body>
</html>