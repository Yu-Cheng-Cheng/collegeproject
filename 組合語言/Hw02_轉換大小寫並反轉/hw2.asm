TITLE HOMEWORK2  (EXE) Repetitive move operations
	.MODEL SMALL
	
STACK	SEGMENT	PARA	STACK	'stack'
	DW	32	DUP(?)
STACK	ENDS

DATASG	SEGMENT	PARA	'data'
KB_INPUT	DB	21 DUP(0)
STR2	DB	21 DUP (0),'$'
DATASG	ENDS

CODESG	SEGMENT	PARA	'code'
MAIN	PROC   FAR
	ASSUME SS:STACK, CS:CODESG, DS:DATASG, ES:DATASG
	MOV	AX,DATASG	;設定DATA的段落
	MOV	DS,AX		;AX暫存器
	MOV	ES,AX

	MOV 	AH,3FH		;輸入指令
	MOV	BX,00H		;file handle
	MOV	CX,21		;設定大小
	LEA	DX,KB_INPUT	;輸入存入字串
	INT	21H		;執行

	MOV	CX,AX		;設定計數字元長度的次數
	LEA	SI,KB_INPUT	;分別放入字串的位址
	LEA	DI,STR2
	DEC	CX
	ADD	DI,CX
	
A20:
	
	MOV	AL,[SI]		;從SI指標得到KB_INPUT的值

	CMP	AL,'A'		;比較字元是不是英文字母
	JB	A30		;如果不是的話跳過
	CMP	AL,'z'
	JA	A30
	CMP	AL,'a'
	JAE	Letter		;是的話轉換大小寫
	CMP	AL,'Z'		
	JA	A30		;_____________________

Letter:	
	XOR AL,20H		;轉換大小寫
	JMP A30

A30: 
	MOV	[DI],AL		;再放入STR2的尾端
	INC	SI		;將位址移到STR1的下一個字母
	DEC	DI
	DEC	CX		;計數減一
	JNZ	A20		;假如cx還沒等於0跳回A20

	MOV	AH,09H		;請求顯示
	LEA	DX,STR2		;將STR2放入值
	INT	21H

	MOV	AX,4C00H	;結束
	INT	21H
MAIN	ENDP
CODESG	ENDS
	END MAIN