TITLE HOMEWORK1  (EXE) Repetitive move operations
	.MODEL SMALL
	
STACK	SEGMENT	PARA	STACK	'stack'
	DW	32	DUP(?)
STACK	ENDS

DATASG	SEGMENT	PARA	'data'
STR1	DB	'multimedia'
STR2	DB	10 DUP ('*'), '$'
DATASG	ENDS

CODESG	SEGMENT	PARA	'code'
MAIN	PROC   FAR
	ASSUME SS:STACK, CS:CODESG, DS:DATASG, ES:DATASG
	MOV	  AX,DATASG	;設定DATA的段落
	MOV	  DS,AX		;AX暫存器
	MOV	  ES,AX

	MOV	  BX,10		;設定計數10次
	LEA	  SI,STR1	;分別放入兩個字串的位址
	LEA	  DI,STR2	
	
A20:
	MOV	  AL,[SI]		;從SI指標得到STR1的值
	MOV	  [DI+BX-1],AL		;再放入STR2的尾端
	INC	  SI		;將位址移到STR1的下一個字母
	DEC	  BX		;計數減一
	JNZ	  A20		;zero glag是否為0 不是的話跳到A20
	
	MOV	  AH,09H		;請求顯示
	LEA	  DX,STR2	;將STR2放入值
	INT	  21H

	MOV	  AX,4C00H	;結束
	INT	  21H
MAIN	ENDP
CODESG	ENDS
	END MAIN