TITLE HOMEWORK3  (EXE) 
	.MODEL SMALL
	
STACK	SEGMENT	PARA	STACK	'stack'
	DW	32	DUP(?)
STACK	ENDS

DATASG	SEGMENT	PARA	'data'
STR1	DB	'A/B+C*D = ?  (0~99)',0DH,0AH,'$'
STR2	DB	'Input A: ','$'
STR3	DB	'Input B: ','$'
STR4	DB	'Input C: ','$'
STR5	DB	'Input D: ','$'
STR6	DB	0DH,0AH,'ERROR! B cannot be a zero','$'
STR7	DB	'Only can input number 0~99',0DH,0AH,'$'
STR8	DB	'Output quotient = ','$'
NUMBER	DB	10 DUP(0)
BINVAL	DW	0
TMPVAL	DW	0
TMPVAL2	DW	0
MULTFACT	DW	1
FACTOR	DB	10
DATA_OUT	DB	4 DUP(0),'$'
DATASG	ENDS

CODESG	SEGMENT	PARA	'code'
MAIN	PROC   FAR
	ASSUME SS:STACK, CS:CODESG, DS:DATASG, ES:DATASG
	MOV	AX,DATASG			;設定DATA的段落
	MOV	DS,AX				;AX暫存器
	MOV	ES,AX	
	
	MOV AH,09H
	LEA DX,STR1				;顯示 str1
	INT 21H
	
	MOV AH,09H
	LEA DX,STR2				;顯示 str2
	INT 21H
	
	CALL	Input			;呼叫 input
	
	MOV	AX,BINVAL			;將 A
	MOV	TMPVAL,AX			;放入TMPVAL
	MOV	BINVAL,0			;歸零儲存輸入的值
	
	MOV AH,09H
	LEA DX,STR3				;顯示 str3
	INT 21H
	
	CALL	Input			;呼叫 input
	CALL	CHECK
	CALL	DIV_NUMBER		;呼叫 除法的副程式，將儲存 A 的TMPVAL 除以儲存 B 的BINVAL
	
	MOV	BINVAL,0			;歸零儲存輸入的值
	
	
	MOV AH,09H
	LEA DX,STR4				;顯示 str4
	INT 21H
	
	CALL	Input			;呼叫 input
	
	MOV	AX,BINVAL			;將 C
	MOV	TMPVAL2,AX			;放入TMPVAL2
	MOV	BINVAL,0			;歸零儲存輸入的值
	
	MOV AH,09H
	LEA DX,STR5				;顯示 str5
	INT 21H
	
	CALL	Input			;呼叫 input
	CALL	MUL_NUMBER		;呼叫 乘法的副程式，將儲存 C 的TMPVAL2 乘上儲存 D 的BINVAL
	
	MOV	AX,TMPVAL			;把除法的結果放入 AX
	ADD	TMPVAL2,AX			;加上乘法的結果
	
	CALL	Output			;將結果轉換成 ASCII 數字
	
	MOV AH,09H
	LEA DX,STR8				;顯示 str8
	INT 21H
	
	MOV	AH,09H				;請求顯示
	LEA	DX,DATA_OUT			;將STR2放入值
	INT	21H

	MOV	AX,4C00H			;結束
	INT	21H
MAIN	ENDP
;
Input	PROC	NEAR
		MOV	AH,3FH			;interrupt 31H
		MOV	BX,00H			;file handle 00H
		MOV	CX,10			;max-len = 4
		LEA	DX,NUMBER		;給輸入位址
		INT 21H				;執行
	
		CMP AX,4			;輸入值長度超過2
		JA	A20
	
		MOV	CX,AX			;輸入值的長度放入CX
		LEA	SI,NUMBER		;輸入值的位址放入SI
		ADD	SI,AX			;從個位數字開始擺放
		DEC SI			
		DEC SI				;有兩次是因為要扣掉 0AH,0DH
		DEC SI				;一次是因為 0 開始
		DEC CX
		DEC	CX				;有兩次是因為要扣掉 0AH,0DH
	
A10:	MOV	AL,[SI] 		;將SI的值放入AL
		AND	AX,000FH		;將AX 中字元的值變成個位數字
		MUL	MULTFACT		;乘上基數一開始是1, 之後會變成10, 100, 1000...
		ADD	BINVAL,AX		;將乘過後的值加入儲存的資料
		MOV	AX,MULTFACT		;將基數
		MUL	FACTOR			;乘10
		MOV	MULTFACT,AX		;乘完要把它擺回去
		DEC	SI				;SI 減一，從個位取值變成十位取值
		DEC CX				;迴圈減一
		JNZ	A10				;CX 不等於 0 迴圈 A10 
		
		CMP BINVAL,0		;比較 BINVAL 和 0
		JB	A20				;小於的話跳至 A20
		JMP A30				;比較都沒成立就跳至 A30
		
A20:	MOV AH,09H
		LEA DX,STR7			;顯示錯誤資訊
		INT 21H
		MOV	AX,4C00H			;結束程式
		INT	21H
		
A30:
		MOV	MULTFACT,1		;計算完 基數變回原樣
		RET
Input	ENDP
;
Output	PROC	NEAR
		MOV	CX,10			;把 10 放入 CX
		LEA	SI,DATA_OUT+3	;要從個位數字位開始擺放
		MOV	AX,TMPVAL2		;將除法乘法相加後的結果放入 AX
C10:	CMP	AX,CX			;比較 AX, CX
		JB	C20				;小於的話跳至 C20
		XOR	DX,DX			;若沒有的話 擴充 AX 至 AX:DX 
		DIV	CX				;除以10
		OR	DL,30H			;將餘數變成 ASCII 字元
		MOV	[SI],DL			;將字元放入儲存資料的個位數字
		DEC	SI				;把儲存資料加一位
		JMP	C10				;迴圈到 C10
		
C20:	OR	AL,30H			;將最後一個數字變成 ASCII 字元
		MOV	[SI],AL			;擺入儲存資料
		
		RET
Output	ENDP
;
DIV_NUMBER	PROC	NEAR
		MOV	AX,BINVAL		;檢查用
		MOV	AX,TMPVAL		;將 A 放置AX
		CMP	AX,BINVAL		;A 和 B 比較
		JAE	B10				;大於等於的話跳至 B10
		DIV	BINVAL			;A 除 B
		MOV	AH,00			;將 AH 歸零
		MOV	TMPVAL,AX		;將得到的值放回 TMPVAL
		JMP	B30				;跳至B30
		
B10:	XOR	DX,DX			;擴充 AX 至 AX:DX
		MOV	CX,BINVAL		;將 B 放入 CX
		DIV	CX				;A 除 B
		MOV	TMPVAL,AX		;將得到的值放回 TMPVAL
	
B30:	
		RET
DIV_NUMBER	ENDP
;
MUL_NUMBER	PROC	NEAR

		MOV	AX,TMPVAL2		;將 C 放入 AX
		MUL	BINVAL			;乘上 D
		MOV	TMPVAL2,AX		;將得到的值放回 TMPVAL2
		
		RET
MUL_NUMBER	ENDP	
;
CHECK	PROC	NEAR

		CMP	BINVAL,0		;比較 BINVAL 和 0
		JNE	D10				;不等於跳至 D10
		MOV AH,09H			
		LEA DX,STR6			;顯示錯誤資訊
		INT 21H
		MOV	AX,4C00H			;結束程式
		INT	21H
		
D10:	
		RET
CHECK	ENDP
;
CODESG	ENDS
	END MAIN