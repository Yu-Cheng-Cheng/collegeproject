package normal;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class normal_sol {

	public static void main(String[] args) throws IOException {
	
		long startTime = System.currentTimeMillis();	//紀錄開始時間
		
		FileReader fr = new FileReader("input.txt");		//將檔案讀進FileReader
		
		BufferedReader br = new BufferedReader (fr);		//用BufferedReader去接FileReader讀到的檔案
		
		ArrayList storeData =new ArrayList();		//創一個動態陣列
	
		String line;		
		
		while((line = br.readLine()) != null)		//用宣告的SRING 去接檔案中一行的值
			storeData.add(line);		//由於一行只讀到一個數字，直接把它放進動態陣列裡
		
		double	store_data[]= new double[200],	//宣告放讀進資料的陣列store_data
							data[] = new double[200];		//宣告放差值的陣列data

		int data_length = Integer.parseInt((String) storeData.get(0));		//data_length 存放第一個store_data讀到的 STRING 後，轉成INT，即是有幾個檔案

		store_data[0] = Double.parseDouble((String) storeData.get(1));		//先把第二個 store_data 讀到的STRING資料轉成 DOUBLE 後，即為第一天放入store_data的第一個以便計算

				
				//迴圈計算前後兩天差值後，丟進DATA
		for(int i = 0; i < data_length -1 ; i++){

			store_data[i+1] =  Double.parseDouble((String) storeData.get(i+2));

			data[i] = store_data[i+1] - store_data[i];		

		}
		
		double max = 0, temp = 0;		
		
		int left = 0, right = 0;
		
		
			//用雙層迴圈從第0天到最後一天為基準，計算接下來之後幾天的合計，並把最大值放入MAX中
		for(int i = 0; i < data_length-1; i++){
			
			for(int j = i ; j < data_length-1; j++){
						
				temp = temp +data[j];		//用 TEMP 放合計
			
				//TEMP 比目前 MAX 高就取代 MAX，並記錄下以哪天為基準，算到哪天為最大值
				if (temp > max){		
					max = temp;
					left = i;
					right = j;				
				}	
			}	
			
			temp = 0;		//歸零TEMP，以便下次回圈不會出錯
			
		}
		
		
		System.out.println("第 "+left+ " 日收盤買進，第 "+(right+1)+"日收盤賣出，最高獲利為  "+max +" 元");
		
		System.out.println("執行時間: " + (System.currentTimeMillis() - startTime) + " ms"); //印出 現在時間 - 開始時間
	}

}
