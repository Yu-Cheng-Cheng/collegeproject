package mergeSort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class mergeSort {

	//由於副程式只能回傳一個值，設全域變數存放另外二值
	static double left_low = 0,		//最大值的低位天
									right_max = 0,		//最大值的高位天
									left[] = new double [2],		//用來存放切割後左邊陣列的買進日期、賣出日期
									right[] = new double[2],		//用來存放切割後右邊陣列的買進日期、賣出日期
									cross[] = new double[2],	//用來存放CROSS後陣列的買進日期、賣出日期
									final_day[] = new double[2];
	
	public static void main(String[] args) throws IOException{
		
		long startTime = System.currentTimeMillis();	//紀錄開始時間
		
		FileReader fr = new FileReader("input_2330.txt"); 	//將檔案讀進FileReader
		
		BufferedReader br = new BufferedReader (fr);		//用BufferedReader去接FileReader讀到的檔案
		
		ArrayList storeData =new ArrayList();		//創一個動態陣列
	
		String line;
		
		while((line = br.readLine()) != null)		//用宣告的SRING 去接檔案中一行的值
			storeData.add(line);		//由於一行只讀到一個數字，直接把它放進動態陣列裡
			
		double	store_data[]= new double[200],			//宣告放讀進資料的陣列store_data
							data[] = new double[200];			//宣告放差值的陣列data
		
		int data_length = Integer.parseInt((String) storeData.get(0));		//data_length 存放第一個store_data讀到的 STRING 後，轉成INT，即是有幾個檔案
		
		store_data[0] = Double.parseDouble((String) storeData.get(1));		//先把第二個 store_data 讀到的STRING資料轉成 DOUBLE 後，即為第一天放入store_data的第一個以便計算

		
		//迴圈計算前後兩天差值後，丟進DATA
		for(int i = 0; i < data_length -1 ; i++){
			
			store_data[i+1] =  Double.parseDouble((String) storeData.get(i+2));
			
			data[i] = store_data[i+1] - store_data[i];		
		
		}
			
		double max = find_max_subarray(data, 0,data_length );		//呼叫函式開始遞迴得到最大值;
		
		System.out.println("第 "+final_day[0]+ " 日收盤買進，第 "+(final_day[1]+1)+"日收盤賣出，最高獲利為  "+max +" 元");
			
		System.out.println("執行時間: " + (System.currentTimeMillis() - startTime) + " ms"); //印出 現在時間 -開始時間
	}
	
	
	//用來計算 CROSS 的最大值
	public static double find_cross_max_subarray(double[]i, int low, int mid, int high){
		
		double left_sum_in = -1,		//存放左邊最大的和
						right_sum_in = -1,		//存放右邊最大的和
						sum_in= 0;		//暫存的和
			
		//從 mid 往左邊加，存入sum_in，若比 left_sum_in 大就放進去，並把這時第幾天放入低位天 left_low;
		for ( int x = mid; x > low; x--){
			
			sum_in += i[x];		
			
			if(sum_in > left_sum_in){
				left_sum_in = sum_in;	
				left_low = x;
			}
		}
		
		sum_in = 0;		//歸零暫存值
		
		
		//從 mid 往右邊加，存入sum_in，若比 right_sum_in 大就放進去，並把這時第幾天放入高位天  right_max;
		for ( int x = mid + 1; x < high; x++){
			
			sum_in += i[x];
			
			if(sum_in > right_sum_in){
				right_sum_in = sum_in;	
				right_max = x;
			}
		}
		
		return right_sum_in + left_sum_in; //將左邊的最大的和+右邊最大的和回傳，即為CROSS的最大和
	}

	public static  double  find_max_subarray(double[] i , int low, int high){
		
		if (low == high)		//如果資料只有一個就回傳那一個
			return i[low];

		else{
			
			int mid = (low + high) / 2; 		//int 將有小數點的 mid 就是無條件捨去小數點
			
			double left_sum = find_max_subarray(i, low, mid);		//呼叫自己從中間算到最低位計算之間的最大值
			
			left[0] = left_low; left[1] = right_max;		//將得到最大值中，低位的是第幾天放入LEFT 的第一個數，高位的是第幾天放入LEFT 的第二個數
			
			double right_sum = find_max_subarray(i, mid + 1, high);		//呼叫自己從中間算到最高位計算之間的最大值 
			
			right[0] = left_low; right[1] = right_max;		//將得到最大值中，低位的是第幾天放入right 的第一個數，高位的是第幾天放入LEFT 的第二個數
			
			double cross_sum = find_cross_max_subarray(i, low, mid, high);		//呼叫自己從最低位到最高位計算之間的最大值
			
			cross[0] = left_low; cross[1] = right_max;		//將得到最大值中，低位的是第幾天放入cross 的第一個數，高位的是第幾天放入cross 的第二個數
			
			
			//判斷是 從 mid 切割後左邊陣列比較大還是右邊比較大，而且還要比穿越左右陣列的和大，把最後結果回傳，低位天和高位天放入final_day
			if(left_sum > right_sum && left_sum > cross_sum){
				
				final_day[0] = left[0]; final_day[1] = left[1];

				return left_sum;
			}
			else if(right_sum > left_sum && right_sum > cross_sum){
				
				final_day[0] = right[0]; final_day[1] = right[1];

				return right_sum;
			}
			else{
				
				final_day[0] = cross[0]; final_day[1] = cross[1];

				return cross_sum;
			}
		}
	}

}
