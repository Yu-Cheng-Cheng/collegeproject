import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BD {
	
	static int[] best = new int[4096]; 		//存放最佳解
	static int[] cut = new int[4096]; 		//存放在哪切割
	
	public static void main(String[] args) throws IOException{
		
		FileReader fr = new FileReader("p2.txt");		//將檔案讀進FileReader
	
		BufferedReader br = new BufferedReader (fr);		//用BufferedReader去接FileReader讀到的檔案
		
		ArrayList storeData =new ArrayList();		//創一個動態陣列
	
		 String[] tempArray= new String[2];
		
		String line, tempstring;		
		
		while((line = br.readLine()) != null)		//用宣告的SRING 去接檔案中一行的值
		{	
			tempstring = line;
			 tempArray = tempstring.split("\\s");
			 storeData.add(tempArray[1]);		//由於一行只讀到一個數字，直接把它放進動態陣列裡
		}
		System.out.println("Rod price");
		System.out.println(storeData);
		
		System.out.print("Please keyin what length you want to calulate : ");
		Scanner input = new Scanner(System.in);	
		int n = input.nextInt();
		
		int data_store[] = new int[4096];
		
		int data_len = storeData.size();		//data_length 存放第一個store_data讀到的 STRING 後，轉成INT，即是有幾個檔案

		for(int i = 0; i < data_len; i++)
			data_store[i] =Integer.parseInt((String) storeData.get(i)) ;
		
		long startTime = System.currentTimeMillis();	//紀錄開始時間
		int max = bottomUpCutRod(data_store, n);		//開始計算
		startTime =  System.currentTimeMillis() - startTime;		//將現在時間 - 開始時間 = cutRod的運算時間
		
		System.out.println("Maximum revenue : " + max);		//印出max
		printCutRodSol(data_store, n);		//印出在哪一段切割
		System.out.println("Mehod : BD   執行時間: " + startTime + " ms"); //印出 現在時間 -開始時間
		
	}
	
	static int bottomUpCutRod(int[] p, int n)
	{
		best[0] = 0;		//將長度為 0 的最佳解初始為 0
		
		for(int j = 1; j <= n; j++)		//由長度為 1 開始計算到長度為 n
		{
			int q = -1;		//用來存放目前最大值，初始為-1 以便確認有數比他大
			for (int i = 1; i <= j ; i++)		//計算每個長度 j 中的排列組合
				if(q < p[i -1] + best[j - i] )		//若有任何在 j 的組合算出的價錢比目前儲存的 q 還大
				{
					q = p[i - 1] + best[j - i];		//就取代他
					cut[j] = i;			//並將此時長度為 j 在 i 被切割存在 cut 陣列中
				}
			best[j] = q;		//把長度為 j 的最佳值存下來
		}
		
		return best[n];		//回傳最佳值
	}
	
	static void printCutRodSol (int[] p , int n)		//印出在哪cut
	{
		System.out.print("Cut with : ");
		while(n > 0)
		{
			System.out.print(cut[n]+" ");		//將 n 在哪 cut 印出
			n = n -cut[n];		//值算 n 被 cut 後的值，在迴圈直到 n < 0
		}
		System.out.println();
	}
}
