import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BF {

	public static void main(String[] args) throws IOException  {
		// TODO 自動產生的方法 Stub
		
		FileReader fr = new FileReader("p2.txt");		//將檔案讀進FileReader
		
		BufferedReader br = new BufferedReader (fr);		//用BufferedReader去接FileReader讀到的檔案
		
		ArrayList storeData =new ArrayList();		//創一個動態陣列
	
		 String[] tempArray= new String[2];
		
		String line, tempstring;		
		
		while((line = br.readLine()) != null)		//用宣告的SRING 去接檔案中一行的值
		{	
			tempstring = line;
			 tempArray = tempstring.split("\\s");		//以空白為界切割街道的字串
			 storeData.add(tempArray[1]);		//將切出來的第二個字串拿出來即為價目表
		}
		System.out.println("Rod price");
		System.out.println(storeData);
		
		int data_store[] = new int[4096];		//用來儲存轉換成INT的價目表
		
		int data_len = storeData.size();		//data_length 存放第一個store_data讀到的 STRING 後，轉成INT，即是有幾個檔案

		for(int i = 0; i < data_len; i++)		//把動態陣列傳換成一般INT陣列
			data_store[i] =Integer.parseInt((String) storeData.get(i)) ;
			
		System.out.print("Please keyin what length you want to calulate : ");		//輸入鋼筋長度
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
	
		long startTime = System.currentTimeMillis();	//紀錄開始時間
		int max = cutRod(data_store, n);		//開始計算
		startTime =  System.currentTimeMillis() - startTime;	//將現在時間 - 開始時間 = cutRod的運算時間
		
		System.out.println("Maximum revenue : " + max );		//印出max
		System.out.println("Mehod : BF   執行時間: " + startTime + " ms"); //印出 現在時間 -開始時間
	}

	public static int cutRod (int[] p, int n )
	{
		if (n == 0)		//遇到 n=0 時回傳0
			return 0;
		
		int q = -1; 		//用來存放目前最大值，初始為-1 以便確認有數比他大
		
		for( int i = 1; i <= n ; i++)		//將 1~n 都跑過一遍
		{
			if (q  <  p[i - 1] + cutRod(p, n - i))		//若有任何在 n 的組合算出的價錢比目前儲存的 q 還大
				q =  p[i - 1] + cutRod(p, n  - i);		//就取代他	
		}
		return q;		//最終回傳的即為最佳價錢
	}
}
