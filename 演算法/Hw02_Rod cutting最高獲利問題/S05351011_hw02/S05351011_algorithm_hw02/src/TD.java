
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TD {

	static int[] best = new int[4096]; 
	static int[] cut = new int[4096]; 
	static int temp = 1;
	
	public static void main(String[] args) throws IOException  {
		// TODO 自動產生的方法 Stub
		
		FileReader fr = new FileReader("p2.txt");		//將檔案讀進FileReader
		
		BufferedReader br = new BufferedReader (fr);		//用BufferedReader去接FileReader讀到的檔案
		
		ArrayList storeData =new ArrayList();		//創一個動態陣列
	
		 String[] tempArray= new String[2];
		
		String line, tempstring;		
		
		while((line = br.readLine()) != null)		//用宣告的SRING 去接檔案中一行的值
		{	
			tempstring = line;
			 tempArray = tempstring.split("\\s");
			 storeData.add(tempArray[1]);		//由於一行只讀到一個數字，直接把它放進動態陣列裡
		}
		System.out.println("Rod price");
		System.out.println(storeData);
		
		int data_store[] = new int[4096];
		
		int data_len = storeData.size();		//data_length 存放第一個store_data讀到的 STRING 後，轉成INT，即是有幾個檔案

		for(int i = 0; i < data_len; i++)
			data_store[i] =Integer.parseInt((String) storeData.get(i)) ;
			
	
		System.out.print("Please keyin what length you want to calulate : ");		//輸入鋼筋長度
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		
		long startTime = System.currentTimeMillis();	//紀錄開始時間
		int max = cutRod(data_store, n);		//開始計算
		startTime =  System.currentTimeMillis() - startTime;		//將現在時間 - 開始時間 = cutRod的運算時間
		
		System.out.println("Maximum revenue : " + max);		//印出max
		printCutRodSol(data_store, n);		//印出在哪一段切割
		System.out.println("Mehod : TD   執行時間: " + startTime + " ms"); //印出 現在時間 -開始時間

	}

	public static int cutRod (int[] p, int n )
	{
		int q ; 		//宣告 q 來存放最大值
		
		if (best[n] > 0)		//若遇到已算過的最佳 n 時，直接回傳算最佳值
			return best[n];
		
		if (n == 0)		//遇到 n=0 時回傳0
			return 0;
		else
		{ 
			q = -1;		//設為-1 以便確認有數比他大
			for( int i = 1; i <= n ; i++)		//將 1~n 都跑過一遍
				if (q  <  p[i - 1] + cutRod(p, n - i))			//若有任何在 n 的組合算出的價錢比目前儲存的 q 還大
				{
					q =  p[i - 1] + cutRod(p, n -i);		//就取代他
					cut[temp] = i;		//並將此時長度為 temp 在 i 被切割存在 cut 陣列中
				}	
		}
		temp++;		//迴圈結束時就 +1 
		best[n] = q;		//把算出的最佳值存下來
		return q;		//回傳最佳值
	}
	
	static void printCutRodSol (int[] p , int n)		//印出在哪cut
	{
		System.out.print("Cut with : ");
		while(n > 0)
		{
			System.out.print(cut[n]+" ");		//將 n 在哪 cut 印出
			n = n -cut[n];		//值算 n 被 cut 後的值，在迴圈直到 n < 0
		}
		System.out.println();
	}
}
