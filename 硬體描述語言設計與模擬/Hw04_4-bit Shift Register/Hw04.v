module d_ff(
   output reg q,
   input d,
   input clock,reset
);

always@(negedge clock)
   if(reset) 
      q <= 1'b0;
   else
      q <= d;

endmodule;

module shift_register(
   output [3:0] Q,
   input [0:0] D,
   input clk,rst
);

d_ff dff3( Q[0], D[0], clk, rst);
d_ff dff2( Q[1], Q[0], clk, rst);
d_ff dff1( Q[2], Q[1], clk, rst);
d_ff dff0( Q[3], Q[2], clk, rst);

endmodule;

module shif_registe_tb;
   wire [3:0] q_;
   reg [0:0] d_ ;
   reg clk_, rst_;

shift_register sr1(q_, d_, clk_, rst_);

initial clk__ = 1'b0;
always #5 clk_ = ~clk_;
initial begin
   d_ <= 1'b1;
   rst_ <= 1'b1;
   #20 rst_ = 1'b0;
   #20 rst_ = 1'b1;
   #20 rst_ = 1'b0;
   #20 $finish;
end 

initial
 $monitor($time, " Output q = %d", q_);

endmodule;