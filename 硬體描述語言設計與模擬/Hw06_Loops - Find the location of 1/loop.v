module mux_loop;
reg [11:0] flag;
reg count;
integer i;

initial
begin
 i = 11;
 flag = 12'b 0010_0101_1110;
 count = 1'b1;
 while((i >= 0 ) && count )
  begin
  if (flag[i])
   begin
    $display("%d bit", i);
   count = 1'b0;
   end
  i = i - 1;
 end

 count = 1'b1;
 while((i >= 0) && count )
 begin
  if (flag[i])
  begin
   $display("%d bit", i);
   count = 1'b0;
  end
  i = i - 1;
 end

end
endmodule
