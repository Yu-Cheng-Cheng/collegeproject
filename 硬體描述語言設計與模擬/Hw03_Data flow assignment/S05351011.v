module select(
   input [3:0] a,
   input [3:0] b,
   input c,
   output [3:0]d
);

assign d[3:0] = (c == 1) ? a [3:0] + b[3:0] : a [3:0] - b[3:0];

endmodule;


module select_tb;
 reg [3:0] A;
 reg [3:0] B;
 reg C;
 wire [3:0]D;

select Sel(A, B, C, D);

endmodule;