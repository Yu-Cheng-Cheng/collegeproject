module D_FF(
   output reg Q, Q_n,
   input d, CLK, Clear
);

always @(posedge CLK or posedge Clear)
   if (Clear) begin
      Q <= 1'b0;
      Q_n <= 1'b0;
   end
   else begin
      Q <= d;
      Q_n <= ~d;
   end

endmodule 

module shift_register(
   output [3:0] q,
   output [3:0] q_n,
   input clk, clear 
);

D_FF dff3(q[3], q_n[3], q[0], clk, clear);
D_FF dff2(q[2], , q_n[3], clk, clear);
D_FF dff1(q[1], , q[2], clk, clear);
D_FF dff0(q[0], , q[1], d, clk, clear);

endmodule

module shift_register_tb;
   reg clk;
   reg clear;
   wire[3:0] q; 
   wire[3:0] q_n; 

shift_register sh1(q, q_n, clk, clear);

initial clk = 1'b0;
always #5 clk = ~clk;

initial begin
 q_n[3] = 1'b1;
 q[2] = 1'b0;
 q[1] = 1'b0;
 q[0] = 1'b0;
 #25  clear = 1'b1;
 #0 $finish;
end 

endmodule

