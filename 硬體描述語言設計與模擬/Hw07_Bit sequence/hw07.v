`define STATE_BITS 2
`define S0 2'b00
`define S1 2'b01
`define S2 2'b10
`define S3 2'b11

module seq(z, state, reset_n, clk, x);
 output z;
 output [`STATE_BITS:0] state;
 input reset_n;
 input clk;
 input x;
 reg z;
 reg [`STATE_BITS:0] state;

always @(state) begin
 case(state)
  `S0: begin
    z <= 1'b0;
   end
  `S1: begin
    z <= 1'b0;
   end
  `S2: begin
    z <= 1'b0;
   end
  default: begin
    z <= 1'b1;
   end
 endcase
end

always @(negedge reset_n or posedge clk) begin
 if(~reset_n)
  state <= `S0;
 else begin
  case (state)
   `S0: begin
      if (x) 
        state <= `S1;
      else
        state <= `S0;
  end
  `S1: begin
      if (x) 
        state <= `S2;
      else
        state <= `S0;
  end
  `S2: begin
      if (x) 
        state <= `S3;
      else
        state <= `S0;
  end
  default: begin
      if (x) 
        state <= `S3;
      else
        state <= `S0;
  end
  endcase
 end
end
endmodule

`timescale 1ns/1ns
`define PERIOD1 100
`define STATE_BITS 2 
`define S0 2'b00
`define S1 2'b01
`define S2 2'b10
`define S3 2'b11

module seq_tb();
 wire z; 
 wire [`STATE_BITS-1:0] state;
 reg reset_n; 
 reg clk; 
 reg x;
 integer error_count;

 seq uut( z, state, reset_n, clk, x);

 initial begin
  clk = 0; 
  error_count = 0; 
  reset_n = 1;
  #(`PERIOD1/4) reset_n = 0;
  #`PERIOD1 reset_n = 1; 
 end 

 always #(`PERIOD1/2) clk = ~clk;

 task error_check;
  input [`STATE_BITS:0] expected1, actual1;
  input expected2, actual2;
  begin
   if (!((actual1 === expected1) && (actual2 === expected2))) begin
    error_count = error_count + 1;
    $display("ERROR: state = %0b, output = %0b at time %0t.", actual1, actual2, $time);
   end 
  end 
 endtask

 initial begin
  $display ("Start simulation.");
  x = 0; // set initial data value; check reset
  #`PERIOD1; error_check(`S0, state, 1'b0, z);
  x = 0; // next, stay in state `S0
  #`PERIOD1; error_check(`S0, state, 1'b0, z);
  x = 1; // next, go to state `S1
  #`PERIOD1; error_check(`S1, state, 1'b0, z);
  x = 0; // next, stay in state `S1
  #`PERIOD1; error_check(`S0, state, 1'b0, z);
  x = 1; // next, go to state `S2
  #`PERIOD1; error_check(`S1, state, 1'b0, z);
  x = 1; // next, go to state `S1 again
  #`PERIOD1; error_check(`S2, state, 1'b0, z);
  x = 0; // next, go to state `S2
  #`PERIOD1; error_check(`S0, state, 1'b0, z);
  x = 1; // next, go to state `S3
  #`PERIOD1; error_check(`S1, state, 1'b0, z);
  x = 1; // next, go to state `S0 again
  #`PERIOD1; error_check(`S2, state, 1'b0, z);
  x = 1; // next, go to state `S1
  #`PERIOD1; error_check(`S3, state, 1'b0, z);
  x = 1; // next, go to state `S2
  #`PERIOD1; error_check(`S3, state, 1'b0, z);
  x = 1; // next, go to state `S3
  #`PERIOD1; error_check(`S0, state, 1'b0, z);
  x = 0; // next, check for output of 1 in state `S3
  #`PERIOD1; error_check(`S1, state, 1'b0, z);
  #`PERIOD1
  if (error_count == 0)
   $display ("Simulation completed with NO errors.");
  else $display ("ERROR: There were %0d errors.",error_count);
   $finish; // terminate all processes
 end // of main initial block to generate and check test vectors
endmodule
