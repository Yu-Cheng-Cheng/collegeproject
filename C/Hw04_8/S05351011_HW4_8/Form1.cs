﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace S05351011_HW4_8
{
    public partial class Form1 : Form
    {
        Random rand = new Random();

        Button[] buttons = new Button[16];

        int expect = 0;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 4; i++)
            {

                for (int j = 0; j < 4; j++)
                {

                    int index = i + j * 4;

                    buttons[index] = new Button();

                    buttons[index].Text = "";

                    buttons[index].Left = i * buttons[0].Width;

                    buttons[index].Top = j * buttons[0].Height;

                    buttons[index].Click += new EventHandler(this.number_Click);

                }

            }

            this.Controls.AddRange(buttons);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void start_Click(object sender, EventArgs e)
        {

            for (int index = 0; index < 16; index++)
            {

                while (true)
                {

                    buttons[index].Text = (rand.Next(16) + 1).ToString();

                    int exist;

                    for (exist = 0; exist < index; exist++)
                    {

                        if (buttons[index].Text == buttons[exist].Text)
                        {

                            break;

                        }

                    }

                    if (exist == index)
                    {

                        break;

                    }

                }


            }

            expect = 1;

        }

        private void reset_Click(object sender, EventArgs e)
        {

            for (int index = 0; index < 16; index++)
            {

                buttons[index].Text = "";

                buttons[index].Enabled = true;
            }

        }

        private void number_Click(object sender, EventArgs e)
        {

            Button btn = (Button)sender;

            if (btn.Text == expect.ToString())
            {

                btn.Enabled = false;

                expect += 1;

            }

        }

    }

}
