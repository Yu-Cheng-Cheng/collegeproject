// S05351011_依成績為範例101位同學.cpp : 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct student {
	int number;
	int chienese;
	int english;
	int math;
	double average;
};

void printStudentList(struct student*, int);

void sortByAvgDESC(struct student*, int); 

void printMiddleList(struct student*, int);

void printMaxList(struct student*, int);

void printMinList(struct student*, int);

int _tmain(int argc, _TCHAR* argv[])
{
	struct student list[101];

	srand((unsigned)time(NULL));

	for ( int i = 0; i < 101; i++ ) {

		list[i].number = i + 1;
		
		list[i].chienese = rand()%101;

		list[i].english = rand()%101;

		list[i].math = rand()%101;

		list[i].average = ( list[i].chienese + list[i].english + list[i].math ) / 3.0;

	}
	
	printStudentList(list, 101);

	sortByAvgDESC(list, 101);

	printStudentList(list, 101);

	printMiddleList(list, 101);

	printMaxList(list, 101);

	printMinList(list, 101);

	while(1);
	return 0;
}

void printStudentList (struct student* list , int max) {

	printf("編號\t國文\t英文\t數學\t平均\n");

	for ( int i = 0; i < 101; i++ ) {
		
		printf("%d\t%d\t%d\t%d\t%.2f\n", list[i].number, list[i].chienese, list[i].english, list[i].math, list[i].average );

	}
	
	printf("================================================\n");
}

void sortByAvgDESC(struct student* list, int max) {

	for(int max_position = 0; max_position < max; max_position += 1) {
	
		for (int target = max_position + 1; target < max; target += 1) {
		
			if (list[max_position].average < list[target].average) {

				struct student temp = list[max_position];

				list[max_position] = list[target];

				list[target] = temp;

			}
		
		}

	}

}

void printMiddleList(struct student* list, int max ) {

	printf("中位數\n編號\t國文\t英文\t數學\t平均\n");

	printf("%d\t%d\t%d\t%d\t%.2f\n", list[max/2].number, list[max/2].chienese, list[max/2].english, list[max/2].math, list[max/2].average );
	
	printf("================================================\n");

}

void printMaxList(struct student* list, int max ) {

	printf("最高分\n編號\t國文\t英文\t數學\t平均\n");

	printf("%d\t%d\t%d\t%d\t%.2f\n", list[0].number, list[0].chienese, list[0].english, list[0].math, list[0].average );
	
	printf("================================================\n");

}

void printMinList(struct student* list, int max ) {

	printf("最低分\n編號\t國文\t英文\t數學\t平均\n");

	printf("%d\t%d\t%d\t%d\t%.2f\n", list[100].number, list[100].chienese, list[100].english, list[100].math, list[100].average );
	
	printf("================================================\n");

}