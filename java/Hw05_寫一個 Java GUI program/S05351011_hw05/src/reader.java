

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.JFileChooser;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.io.File; 

public class reader {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					reader window = new reader();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public reader() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 658, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(15, 61, 606, 338);
		frame.getContentPane().add(textPane);
		
		JButton btnNewButton = new JButton("\u700F\u89BD");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				
				JFileChooser chooser = new JFileChooser();
	
				int returnVal = chooser.showOpenDialog(null);
				
				textField.setText(chooser.getSelectedFile().getPath());
				
				//--------------------------------------------------------
				
				String S [][] = new String [1024][1024];
				
				//if (returnVal == JFileChooser.APPROVE_OPTION) {
				
					File selectedFile = chooser.getSelectedFile();
					
					InputStream File = new FileInputStream ( selectedFile );
					
					XSSFWorkbook wb = new XSSFWorkbook(File);
				
					XSSFSheet sheet = wb.getSheetAt(0); 
				
					XSSFRow row;
				
					XSSFCell cell;
				
					XSSFCellStyle cellStyle;
				
					Iterator rows = sheet.iterator();
				
					ArrayList<String> inputList = new ArrayList<String>();
				
					int swt = 0, days = 0, count = 0;
				
					double[] d = new double[1024];
				
					for (int i = 0; rows.hasNext(); i++) {
						
						row = (XSSFRow) rows.next();
					
						Iterator cells = row.cellIterator();
					
						for (int j = 0; cells.hasNext(); j++) {
						
							cell = (XSSFCell) cells.next();
						
							if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
							
								S[i][j] = cell.getStringCellValue()  ;
							  
							
							} else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
							
								d[i]=cell.getNumericCellValue(); 
									
								S[i][j]=String.valueOf(d[i]);
							
								}
						
						}
					
					}
				
				
				
				//----------------------------------------------
				
				for (int a = 0;rows.hasNext(); a++ ) {
					
					row = (XSSFRow) rows.next();
					
					Iterator cells = row.cellIterator();
					
					System.out.println("");
				
					for (int b = 0; cells.hasNext(); b++) {
					
						System.out.print(S[a][b]);;
						
					}
					
				}
				
			}
		});
		btnNewButton.setBounds(510, 15, 111, 31);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("\u8DEF\u5F91\uFF1A");
		lblNewLabel.setBounds(15, 23, 71, 23);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(77, 17, 418, 29);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
	}
}
