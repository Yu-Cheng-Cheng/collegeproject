package hw04;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class hw04 {
	
	public static void main(String[] args) throws IOException{
		
		InputStream tempFile = new FileInputStream ( "作業4_sample1.xlsx" );
		
		XSSFWorkbook wb = new XSSFWorkbook(tempFile);
		
		XSSFSheet sheet = wb.getSheetAt(0); 
		
		XSSFRow row;
		
		XSSFCell cell;
		
		XSSFCellStyle cellStyle;
		
		Iterator rows = sheet.iterator();
		
		
		int swt = 0, days = 0;
		
		for (int i = 0; rows.hasNext(); i++) {
			
			row = (XSSFRow) rows.next();
			
			Iterator cells = row.cellIterator();
			
			for (int j = 0; cells.hasNext(); j++) {
				
				cell = (XSSFCell) cells.next();
				
				cellStyle = cell.getCellStyle();
				
				XSSFColor myColor = new XSSFColor(new java.awt.Color(255, 255, 0));
				
				if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
				
					if ( cellStyle.getFillForegroundXSSFColor() == myColor ) {
					
						if ( days > 6) {
						
							swt = 1;
						
						}
					
						else {
							
							days = 0;
						
						}
					
					}
				
					else {
					
						days = days + 1;
					
						System.out.println(days);
					
					}
				}
				
			}
			
		}
		
		if (swt == 0) {
			
			System.out.println("此班表不違反一例一休");
			
		}
		
		else if (swt == 1 ) {
			
			System.out.println("此班表違反一例一休");
			
		}
		
		
		
	}


}
