package ReaderWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReaderWriter {

	public static void main(String[] args) throws IOException {
		
		FileWriter writer = new FileWriter("123.txt");
		
		String test = new String("3asda5sd43a54sd");
		
		writer.write(test);
		
		writer.close();
		
		//---------------------------------------------------------
		
		FileReader reader = new FileReader("123.txt");
		
		char[] data = new char[1024];
		
		int num1 = reader.read(data);
		
		String dataString = new String(data,0,num1);
		
		System.out.println(dataString);
		
		reader.close();
		
		//---------------------------------------------------------
		
		FileOutputStream output = new FileOutputStream("321.txt");
		
		byte[] buf2 = "�ڤ~���i�D�f��".getBytes();
		
		output.write(buf2);
		
		output.close();
		
		//---------------------------------------------------------
		
		FileInputStream input = new FileInputStream("321.txt");
		
		byte[] buf1 = new byte[1000];
		
		int num2 = input.read(buf1);
		
		String bufString = new String(buf1,0,num2);
		
		System.out.println(bufString);
		
		input.close();
	
	}

}
