import java.awt.EventQueue;

import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTable;
import javax.swing.JList;

public class WB {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WB window = new WB();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WB() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 617, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("\u7B2C\u4E00\u984C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				JFrame f = new JFrame("第一題");
				f.setSize(400,300);
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.setVisible(true); 
				Container cp = f.getContentPane();
				String[] columns={"服務類型","時數"};
				
				XSSFWorkbook wk = null;
				
				try {
					wk = new XSSFWorkbook("Service Time Sheet.xlsx");
				} catch (IOException e1) {
					// TODO 自動產生的 catch 區塊
					e1.printStackTrace();
				}

				String tempS [][] = new String [2][30];
				
				double[] tempD = new double [30];
				
				double workTime_1 = 0, workTime_2 = 0, workTime_3 = 0, workTime_4 = 0, workTime_5 = 0;
				
				for (int i = 1; i < 30; i++) {
					
					XSSFSheet sht = wk.getSheetAt(i);
					
					for (int j = 0; j < 3; j++ ) {
						
						int x = 0;
						
						if (j == 1 ) x = 4;
						
						else if (j == 2) x = 2;
					
						XSSFRow row = sht.getRow(10 + 3 * j + x);
						
						XSSFCell cell = row.getCell(2);
						
						if (j == 2) {
							
							tempD [i] = cell.getNumericCellValue();
							
							//System.out.println(tempD[i]);
							
						}
						
						if (j < 2 ){
							
							tempS [j][i] = cell.getRichStringCellValue().getString();
						 
							//System.out.println(tempS[j][i]);
							
						}

						x = 0;
						
					}
					
				}
				
				for(int i = 1; i < 30; i ++ ) {
					
					if (tempS[0][i].equals("Finance")) workTime_1 = workTime_1 + tempD[i];
				
					else if (tempS[0][i].equals("Technical")) workTime_2 = workTime_2 + tempD[i];
					
					else if (tempS[0][i].equals("mechanical")) workTime_3 = workTime_3 + tempD[i];
					
					else if (tempS[0][i].equals("manufacture")) workTime_4 = workTime_4 + tempD[i];
					
					else if (tempS[0][i].equals("education")) workTime_5 = workTime_5 + tempD[i];
					
			}
				
				Object[][] data={
					      {"Finance",workTime_1},
					      {"Technical",workTime_2},
					      {"mechanical",workTime_3},
					      {"manufacture",workTime_4},
					      {"education",workTime_5}};
				JTable jt=new JTable(data,columns);
			    jt.setPreferredScrollableViewportSize(new Dimension(617,418));
			    jt.setFont(new java.awt.Font("新細明體",java.awt.Font.BOLD,16));
			    cp.add(new JScrollPane(jt),BorderLayout.CENTER);
			}
		});
		btnNewButton.setBounds(219, 80, 149, 31);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u7B2C\u4E8C\u984C");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				JFrame f = new JFrame("第二題");
				f.setSize(400,300);
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.setVisible(true); 
				Container cp = f.getContentPane();
				String[] columns={"服務人員","時數"};
				
				XSSFWorkbook wk = null;
				
				try {
					wk = new XSSFWorkbook("Service Time Sheet.xlsx");
				} catch (IOException e1) {
					// TODO 自動產生的 catch 區塊
					e1.printStackTrace();
				}

				String tempS [][] = new String [2][30];
				
				double[] tempD = new double [30];
				
				double workTime_a = 0, workTime_b = 0, workTime_c = 0, workTime_d = 0;
				
				for (int i = 1; i < 30; i++) {
					
					XSSFSheet sht = wk.getSheetAt(i);
					
					for (int j = 0; j < 3; j++ ) {
						
						int x = 0;
						
						if (j == 1 ) x = 4;
						
						else if (j == 2) x = 2;
					
						XSSFRow row = sht.getRow(10 + 3 * j + x);
						
						XSSFCell cell = row.getCell(2);
						
						if (j == 2) {
							
							tempD [i] = cell.getNumericCellValue();
							
							//System.out.println(tempD[i]);
							
						}
						
						if (j < 2 ){
							
							tempS [j][i] = cell.getRichStringCellValue().getString();
						 
							//System.out.println(tempS[j][i]);
							
						}

						x = 0;
						
					}
					
				}
				
				for(int i = 1; i < 30; i ++ ) {
					
					if (tempS[1][i].equals("Sonia")) workTime_a = workTime_a + tempD[i];
				
					else if (tempS[1][i].equals("Jack")) workTime_b = workTime_b + tempD[i];
					
					else if (tempS[1][i].equals("James")) workTime_c = workTime_c + tempD[i];
					
					else if (tempS[1][i].equals("Curry")) workTime_d = workTime_d + tempD[i];
					
				}
				
				Object[][] data={
					      {"Sonia",workTime_a},
					      {"Jack",workTime_b},
					      {"James",workTime_c},
					      {"Curry",workTime_d} };
				JTable jt=new JTable(data,columns);
			    jt.setPreferredScrollableViewportSize(new Dimension(617,418));
			    jt.setFont(new java.awt.Font("新細明體",java.awt.Font.BOLD,16));
			    cp.add(new JScrollPane(jt),BorderLayout.CENTER);
			}
		});
		btnNewButton_1.setBounds(219, 126, 149, 31);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("\u7B2C\u4E09\u984C");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				JFrame f = new JFrame("第三題");
				f.setSize(400,300);
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.setVisible(true); 
				Container cp = f.getContentPane();
				String[] columns={"服務人員","服務類型","時數"};
				
				XSSFWorkbook wk = null;
				
				try {
					wk = new XSSFWorkbook("Service Time Sheet.xlsx");
				} catch (IOException e1) {
					// TODO 自動產生的 catch 區塊
					e1.printStackTrace();
				}

				String tempS [][] = new String [2][30];
				
				double[] tempD = new double [30];
				
				double workTime_11 = 0, workTime_12 = 0, workTime_13 = 0, workTime_14 = 0, workTime_15 = 0;
				
				double workTime_21 = 0, workTime_22 = 0, workTime_23 = 0, workTime_24 = 0, workTime_25 = 0;
				
				double workTime_31 = 0, workTime_32 = 0, workTime_33 = 0, workTime_34 = 0, workTime_35 = 0;
				
				double workTime_41 = 0, workTime_42 = 0, workTime_43 = 0, workTime_44 = 0, workTime_45 = 0;
				
				for (int i = 1; i < 30; i++) {
					
					XSSFSheet sht = wk.getSheetAt(i);
					
					for (int j = 0; j < 3; j++ ) {
						
						int x = 0;
						
						if (j == 1 ) x = 4;
						
						else if (j == 2) x = 2;
					
						XSSFRow row = sht.getRow(10 + 3 * j + x);
						
						XSSFCell cell = row.getCell(2);
						
						if (j == 2) {
							
							tempD [i] = cell.getNumericCellValue();
							
							//System.out.println(tempD[i]);
							
						}
						
						if (j < 2 ){
							
							tempS [j][i] = cell.getRichStringCellValue().getString();
						 
							//System.out.println(tempS[j][i]);
							
						}

						x = 0;
						
					}
					
				}
				
				for(int i = 1; i < 30; i++){
					
					if (tempS[1][i].equals("Sonia")){
						
						if (tempS[0][i].equals("Finance")) workTime_11 = workTime_11 + tempD[i];
						
						else if (tempS[0][i].equals("Technical")) workTime_12 = workTime_12 + tempD[i];
						
						else if (tempS[0][i].equals("mechanical")) workTime_13 = workTime_13 + tempD[i];
						
						else if (tempS[0][i].equals("manufacture")) workTime_14 = workTime_14 + tempD[i];
						
						else if (tempS[0][i].equals("education")) workTime_15 = workTime_15 + tempD[i];
						
					}
					
					else if (tempS[1][i].equals("Jack")){
						
						if (tempS[0][i].equals("Finance")) workTime_21 = workTime_21 + tempD[i];
						
						else if (tempS[0][i].equals("Technical")) workTime_22 = workTime_22 + tempD[i];
						
						else if (tempS[0][i].equals("mechanical")) workTime_23 = workTime_23 + tempD[i];
						
						else if (tempS[0][i].equals("manufacture")) workTime_24 = workTime_24 + tempD[i];
						
						else if (tempS[0][i].equals("education")) workTime_25 = workTime_25 + tempD[i];
						
					}
					
					else if (tempS[1][i].equals("James")){
						
						if (tempS[0][i].equals("Finance")) workTime_31 = workTime_31 + tempD[i];
						
						else if (tempS[0][i].equals("Technical")) workTime_32 = workTime_32 + tempD[i];
						
						else if (tempS[0][i].equals("mechanical")) workTime_33 = workTime_33 + tempD[i];
						
						else if (tempS[0][i].equals("manufacture")) workTime_34 = workTime_34 + tempD[i];
						
						else if (tempS[0][i].equals("education")) workTime_35 = workTime_35 + tempD[i];
						
					}
					
					else if (tempS[1][i].equals("Curry")){
						
						if (tempS[0][i].equals("Finance")) workTime_41 = workTime_41 + tempD[i];
						
						else if (tempS[0][i].equals("Technical")) workTime_42 = workTime_42 + tempD[i];
						
						else if (tempS[0][i].equals("mechanical")) workTime_43 = workTime_43 + tempD[i];
						
						else if (tempS[0][i].equals("manufacture")) workTime_44 = workTime_44 + tempD[i];
						
						else if (tempS[0][i].equals("education")) workTime_45 = workTime_45 + tempD[i];
						
					}
					
				}
				
				Object[][] data={
					      {"Sonia","Finance",workTime_11},{"Sonia","Technical",workTime_12},{"Sonia","mechanical",workTime_13},{"Sonia","manufacture",workTime_14},{"Sonia","education",workTime_15},
					      {"Jack","Finance",workTime_21},{"Jack","Technical",workTime_22},{"Jack","mechanical",workTime_23},{"Jack","manufacture",workTime_24},{"Jack","education",workTime_25},
					      {"James","Finance",workTime_31},{"James","Technical",workTime_32},{"James","mechanical",workTime_33},{"James","manufacture",workTime_34},{"James","education",workTime_35},
					      {"Curry","Finance",workTime_41},{"Curry","Technical",workTime_42},{"Curry","mechanical",workTime_43},{"Curry","manufacture",workTime_44},{"Curry","education",workTime_45},
				};
				JTable jt=new JTable(data,columns);
			    jt.setPreferredScrollableViewportSize(new Dimension(617,418));
			    jt.setFont(new java.awt.Font("新細明體",java.awt.Font.BOLD,16));
			    cp.add(new JScrollPane(jt),BorderLayout.CENTER);
			}
		});
		btnNewButton_2.setBounds(219, 172, 149, 31);
		frame.getContentPane().add(btnNewButton_2);
		
	}
	
	public class JTable1 {
		  JFrame f;
		  public void main(String argv[]) {
		    new JTable1();  
		    }
		  public JTable1() {
		    //Setup JFrame
		    JFrame.setDefaultLookAndFeelDecorated(true);
		    JDialog.setDefaultLookAndFeelDecorated(true);
		    f = new JFrame("JTable Test");
		    f.setSize(400,300); 
		    f.setLocationRelativeTo(null); 
		    Container cp=f.getContentPane();

		    //Build Elements
		    Object[][] data={
		      {"Kelly","Female",new Integer(16),false,"kelly@gmail.com"},
		      {"Peter","Male",new Integer(14),false,"peter@gmail.com"},
		      {"Amy","Female",new Integer(12),false,"amy@gmail.com"},
		      {"Tony","Male",new Integer(49),true,"tony@gmail.com"}};
		    String[] columns={"Name","Gender","Age","Vegetarian","E-mail"};
		    JTable jt=new JTable(data,columns);
		    jt.setPreferredScrollableViewportSize(new Dimension(400,300));
		    cp.add(new JScrollPane(jt),BorderLayout.CENTER);
		    f.setVisible(true); 

		    //Close JFrame        
		    f.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE); 
		    f.addWindowListener(new WindowHandler(f));  
		    }
		  }
		class WindowHandler extends WindowAdapter {
		  JFrame f;
		  public WindowHandler(JFrame f) {this.f=f;}
		  public void windowClosing(WindowEvent e) {
		    int result=JOptionPane.showConfirmDialog(f,
		               "確定要結束程式嗎?",
		               "確認訊息",
		               JOptionPane.YES_NO_OPTION,
		               JOptionPane.WARNING_MESSAGE);
		    if (result==JOptionPane.YES_OPTION) {System.exit(0);}
		    }    
		  }
}
