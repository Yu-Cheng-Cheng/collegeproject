package hw04;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class hw04 {
	
	public static void main(String[] args) throws IOException{
		
		InputStream File = new FileInputStream ( "作業4_sample4.xlsx" );
		
		XSSFWorkbook wb = new XSSFWorkbook(File);
		
		XSSFSheet sheet = wb.getSheetAt(0); 
		
		XSSFRow row;
		
		XSSFCell cell;
		
		XSSFCellStyle cellStyle;
		
		Iterator rows = sheet.iterator();
		
		int swt = 0, days = 0, count = 0;
		
		for (int i = 0; rows.hasNext(); i++) {
			
			row = (XSSFRow) rows.next();
			
			Iterator cells = row.cellIterator();
			
			for (int j = 0; cells.hasNext(); j++) {
				
				cell = (XSSFCell) cells.next();
				
				cellStyle = cell.getCellStyle();
				
				if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
					
					count = count + 1;
					
					System.out.println("count == "+count);
					
					if (count == 7) {
						
						count = 0;
	
					}
					
					if ( cellStyle.getFillForegroundXSSFColor() != null ) {
						
						String tempS = String.valueOf(cellStyle.getFillForegroundXSSFColor().getARGBHex());
						
						if ( tempS.equals("FFFF0000")){
							
							days = days + 1;
							
							System.out.println("workday == " + days);
							
						}
						
						else if ( tempS.equals("FFFFFF00")) {
						
							if ( days > 6) {
							
								swt = 1;
							
							}
						
							else {
							
								days = 0;
							
							}
						
						}
						
					}
				
					else {
					
						days = days + 1;
					
						System.out.println("days == "+days);
					
					}
					
				}
				
			}
			
		}
		
		if ( days > 6) {
			
			swt = 1;
		
		}
		
		if (swt == 0) {
			
			System.out.println("此班表不違反一例一休");
			
		}
		
		else if (swt == 1 ) {
			
			System.out.println("此班表違反一例一休");
			
		}
		
		
		
	}


}
